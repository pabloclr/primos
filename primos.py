#--Pablo Clarke Alvarez
# --Programa algoritmo de numeros primos
# El programa debera recibir un numero no negativo
# introducido por el usuario por el teclado y por pantalla
# deberan aparecer los numeros anterioires primos menores que el

# -Solicitamos un numero al usuario

N1 = int(input('Dame un numero entero no negativo:'))
print("Los numeros primos son:")
if N1 >= 0:
    for i in range (1,N1):
        Primo = True
        limite = 2
        while Primo and limite < i:
            if i % limite == 0:
                Primo = False
            else:
                limite += 1

        if Primo:
            print ( i , end = " ")


else:
    print('Numeros negativos no validos')
